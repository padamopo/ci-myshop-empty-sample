package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.ci.myShop.model.Item;

public class StorageTest {
	public Storage storage = null; 	// 1er attribut global pour réutilisation à plusieurs endroits de la classe et non seulement dans une methode
	public Item initItem;			// 2ème attribut global pour réutilisation à plusieurs endroits de la classe et non seulement dans une methode
	public Shop shopTesting; 
	ArrayList<Item> variable= new ArrayList<Item>(); // avant test 
	
			
	@Before
	public void initItem() {
		storage = new Storage (); 					// creation nouvel objet Storage
		initItem= new Item ("TestedItem",150,500f,245); 	// creation d'objet item avant son instantiation 
		storage.addItem(initItem);					// pour execution pre-test : pour chaque creation d'objet, passage d'initItem en Array List
		shopTesting = new Shop(storage, 100f); 		// pour chaque objet Shop : demande de creation de 2 parametres obligatoires ( sotrage et cash, conformement au constructor Shop dans Shop.java lg 10-14)
		}
	
	@Test
	public void addItem () {
		//Storage storage = new Storage (null); 		// test propose en cours de TP
		//Item itemTeste1=new Item ("test",130,5,100);	// test propose en cours de TP
		//Storage.addItem(itemTeste1);					// test propose en cours de TP
		//Item extractedItem=storage.getItem("test");	// test propose en cours de TP
		assertEquals(1,storage.getItemMap().size());	// testons l'ajout d'1 item dans objet storage
		}
	
	@Test
	public void getItem () {
		Item extractedItem = storage.getItem("TestedItem");
		assertEquals(extractedItem,initItem);		// si parmi les articles (Item) de l'Array List, l'ajout de l'objet initItem dans notre stock (storage) apparaît bien avec la chaine 'testedItem'
	}
	@Test
	public void sell () { // sell teste que l'ajout de TestedItem valant 500 en ligne 23 fasse strictement (sans delta:0.0f) passer notre cash de 100 en ligne 25 à 600 en ligne 44
		assertEquals(600f,Shop.sell("TestedItem"),0.0f);  
	}
	@Test
	public void acquire() { // acquire teste que l'achat de TestedItem valant 500 en ligne 23 fasse strictement (sans delta:0.0f) passer notre cash de 100 en ligne 25 à -400 en ligne 48
		assertEquals(-400f,Shop.acquire("TestedItem"),0.0f); 
		}

}
