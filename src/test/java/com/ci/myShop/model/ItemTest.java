package com.ci.myShop.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class ItemTest {

	@Test
	public void createEmptyItem () {
		Item itemTest= new Item (null, 120, 60, 4); 
		assertNotEquals(null,itemTest);
	}
	
	@SuppressWarnings("unused")
	public void updateParameters () {
		Item itemTest= new Item ("name", 120, 5,10); 
		assertEquals("name", itemTest.getName());
	}
}
