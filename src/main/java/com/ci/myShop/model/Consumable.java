package com.ci.myShop.model;

public class Consumable extends Item {

	private int quantity; 
	public Consumable(String name, int quantity, float price, int nbrElt) {
		super(name, quantity, price, nbrElt);
		this.quantity = quantity;// a inserer dans le CONSTRUCTOR. Permet d'alimenter les Getters Setters en attributs pour un clic-droit et Source > Getters Setters
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public class Paper extends Consumable {
		public Paper(String name, int quantity, float price, int nbrElt) {
		super(name, quantity, price, nbrElt);
		}
		private int itemPerLot;
		public int getItemPerLot() {
		return itemPerLot;
		}
		public String PaperQuality ;
		public float weight; 
	}
}