package com.ci.myShop.controller;

import com.ci.myShop.model.Item;

public class Shop {
	private static Storage storage;
	private static float cash;
	
	
	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}

	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public static float getCash() {
		return cash;
	}
	public static void setCash(float cashY) {
		cash = cashY;
	}
	public static int sell (String name) { // previously: public Item sell (String name) > public static int sell (String name)  
		Item Stock = null;
		if (storage.getItem(name)==null) {
			System.out.println("l'article n'est pas en stock");
		}else {
		Stock=storage.getItem(name);
		setCash (getCash ()+ Stock.getPrice());		// 1ère nouvelle méthode pour ajuster le stock en valeur 
		storage.getItemMap().remove(Stock);			// 2ème nouvelle méthode pour réduire le stock en quantité
		}
		return (int) getCash(); // return Stock > return getCash() > return (int) getCash
	}
	public static int acquire (String name) { 		// nouvelle methode gerant l'acquisition d'articles
		//Item Stock = null;
		//if (storage.getItem(name)!=null) {
			//System.out.println("l'article n'est pas en stock");
		//}else {
		Item Stock=storage.getItem(name);
		setCash (getCash()-Stock.getPrice());		
		storage.getItemMap().add(Stock);			
		//}
		return (int) getCash(); 
	}

	public Boolean buy (Item articleCompare) {
		boolean checkCash=false;
		if (articleCompare.getPrice()<=getCash()){
			System.out.println("Achat possible");
		checkCash=true;
		} else { System.out.println("Achat impossible");
		}
		return checkCash;
	}

	public static Item sell() {
		// TODO Auto-generated method stub
		return null;
	}
}
